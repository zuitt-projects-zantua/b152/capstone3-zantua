import { useState,useEffect,useContext } from 'react';
import UserContext from '../userContext';

import { Table,Button } from 'react-bootstrap';

export default function AdminDashboard(){

	const {user} = useContext(UserContext)

	const [allProducts,setAllProducts] = useState([]);

	//set product isActive = false
	function archive(productId){

		fetch(`https://sheltered-crag-61892.herokuapp.com/products/archive/${productId}`,{

			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			window.location.href = '/products'
		})

	}

	//set product isActive = true
	function activate(productId){

		fetch(`https://sheltered-crag-61892.herokuapp.com/products/activate/${productId}`,{

			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			window.location.href = '/products'
		})

	}

	useEffect(()=>{

		if(user.isAdmin){
			fetch('https://sheltered-crag-61892.herokuapp.com/products/all',{

				headers:{
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {

				setAllProducts(data.map(product => {

					return(
						<tr key={product._id}>
							<td>{product._id}</td>
							<td>{product.name}</td>
							<td>{product.price}</td>
							<td>{product.isActive ? "Active": "Inactive"}</td>
							<td>
								{
									product.isActive
									?<Button variant="danger" className="mx-2" onClick={()=>{archive(product._id)}}>Archive</Button>
									:<Button variant="success" className="mx-2"  onClick={()=>{activate(product._id)}}>Activate</Button>
								}
							</td>
						</tr>
					)
				}))
			})
		}
	},[])


	return (
		<>
			<h1 className="my-5 text-center">Admin Dashboard</h1>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Price</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{allProducts}
				</tbody>		
			</Table>

		</>
	)
}