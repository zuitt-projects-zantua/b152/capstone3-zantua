import { Row, Col } from 'react-bootstrap'

export default function Banner({bannerProp}){

	return (
			<Row>
				<Col className="p-5 mt-5">
					<h1 className="mb-3">{bannerProp.title}</h1>
					<h3 className="my-3">{bannerProp.description}</h3>
					<a href={bannerProp.destination} className="btn btn-primary">{bannerProp.buttonText}</a>					
				</Col>
			</Row>
	)
}