import { useState,useContext,useEffect } from 'react';
import {Table} from 'react-bootstrap';

import UserContext from '../userContext';

export default function Orders(){

	const {user} = useContext(UserContext)

	const [allOrders,setAllOrders] = useState([]);

	if(user.isAdmin){

		//Admin - all orders
		function GetAllOrders(){
		useEffect(()=>{

			fetch('https://sheltered-crag-61892.herokuapp.com/orders/getAllOrders',{
				method: 'GET',
				headers: {
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {

				setAllOrders(data.map(order =>{

					let orderProducts = order.products
					
					return(
						<tr key={order._id}>
							<td>{order._id}</td>
							<td>{order.userId}</td>
							<td>
							{orderProducts.map(result =>{
								return (<p key={result.productId}>{result.productId}</p>)
							})}
							</td>
							<td>{orderProducts.map(result =>{
								return (<p key={result.productId}>{result.quantity}</p>)
							})}</td>
							<td>{order.purchasedOn.slice(0,10)}</td>
							<td>P{order.totalAmount}</td>						
						</tr>

					)
				}))
			})
		},[])
		}
		GetAllOrders()
	}
	else{

		//user - orders
		function GetUserOrders(){
		useEffect(()=>{

			fetch('https://sheltered-crag-61892.herokuapp.com/orders/getUserOrders',{
				headers: {
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {

				setAllOrders(data.map(order =>{

					let orderProducts = order.products

					return(
						<tr key={order._id}>
							<td>
								{orderProducts.map(result =>{
									return (<p key={result.productId}>{result.productId}</p>)
								})}
							</td>
							<td>
								{orderProducts.map(result =>{
									return (<p key={result.productId}>{result.quantity}</p>)
								})}
							</td>
							<td>{order.purchasedOn.slice(0,10)}</td>
							<td>P{order.totalAmount}</td>
							
						</tr>

					)
				}))
			})
		
		},[])
		}
		GetUserOrders()
	}


	return(
		<>
			<h1 className="my-5 text-center">Orders</h1>
			<Table striped bordered hover>
				<thead>
					<tr>
						{
							user.isAdmin
							?<th>Order Id</th>
							:<></>
						}
						{
							user.isAdmin
							?<th>User Id</th>
							:<></>
						}
						<th>Product Id</th>
						<th>Quantity</th>
						<th>Purchased On</th>	
						<th>Total Amount</th>				
						
					</tr>
				</thead>
				<tbody>
						{allOrders}
				</tbody>		
			</Table>
		</>
	)
}