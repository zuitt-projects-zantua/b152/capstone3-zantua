import { useEffect,useState } from 'react';
import { Row,Container } from 'react-bootstrap';

import Banner from '../components/Banner';
import ProductCard from '../components/Product';

export default function Home(){

	let bannerData = {
		title: "FanShop",
		description: "Your one and Only Fans store for you ventilation needs. Providing excellent products and services worldwide. Wide variety of products all at affordable prices. ",
		destination: "/products",
		buttonText: "View Catalog"
	}

	const [productArray1,setProductArray1] = useState([])
	const [productArray2,setProductArray2] = useState([])

	//products with most orders
	useEffect(()=>{

		fetch("https://sheltered-crag-61892.herokuapp.com/products/hot")
		.then(res => res.json())
		.then(data => {

			setProductArray1(data.map(product =>{

				return (

					<ProductCard key={product._id} productProp={product} />
				)
			}))
		})
	},[])
	
	//products most recently added into database
	useEffect(()=>{

		fetch("https://sheltered-crag-61892.herokuapp.com/products/new")
		.then(res => res.json())
		.then(data => {

			setProductArray2(data.map(product =>{

				return (

					<ProductCard key={product._id} productProp={product} />
				)
			}))
		})
	},[])

	
	return (
		<>	
			<Banner bannerProp={bannerData} />
			<Container fluid>
			<h1 id="hot" className="my-3">Hot</h1>
			
			<Row className="d-flex justify-content-between">
			
			{productArray1}
			
			</Row>
			
			<h1 id="new" className="mt-5 mb-3">New Arrivals</h1>
			<Row className="d-flex justify-content-between">
			{productArray2}
			
			</Row>
			</Container>
			
		</>
	)
}