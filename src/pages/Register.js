import { useState,useContext,useEffect } from 'react';
import { Navigate,Link } from 'react-router-dom';

import { Form,Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

import UserContext from '../userContext';


export default function Register(){

	const {user,setUser} =useContext(UserContext)

	const [firstName,setFirstName] = useState("");
	const [lastName,setLastName] = useState("");
	const [email,setEmail] = useState("");
	const [mobileNo,setMobileNo] = useState("");
	const [password,setPassword] = useState("");
	const [confirmPassword,setConfirmPassword] = useState("");

	const [isActive,setIsActive] = useState(false);

	useEffect(()=>{

		if(firstName !== "" && lastName !== "" && email !== "" && mobileNo.length === 11 && password === confirmPassword){
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	},[firstName,lastName,email,mobileNo,password,confirmPassword])

	//register a user
	function registerUser(e){

		e.preventDefault();

		fetch('https://sheltered-crag-61892.herokuapp.com/users/',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
				})
			
			})
			.then(res => res.json())
			.then(data => {

			if(data.email){
				Swal.fire({

					icon:'success',
					title:"Registration Succesful",
					text:`Thank you for registering ${data.firstName}`,
					confirmButtonText: 'Login'
				})
				.then(() =>{
				    window.location = "/login";
				});

			}
			else{
				Swal.fire({

					icon:'success',
					title:"Registration Failed",
					text: data.message
				})
			}
		})

	}

	return(

		user.id
		?
		<Navigate to="/" replace={true} />
		:

		<>
			<h1 className="my-5">Register</h1>
			<Form onSubmit={e => registerUser(e)} className="col-12 col-md-6">
				<Form.Group>
					<Form.Label>First Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter First Name" required value={firstName} onChange={e => {setFirstName(e.target.value)}} />
				</Form.Group>
				<Form.Group>
					<Form.Label>Last Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter Last Name" required value={lastName} onChange={e => {setLastName(e.target.value)}} />
				</Form.Group>
				<Form.Group>
					<Form.Label>Email:</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" required value={email} onChange={e => {setEmail(e.target.value)}} />
				</Form.Group>
				<Form.Group>
					<Form.Label>Mobile No:</Form.Label>
					<Form.Control type="text" placeholder="Enter Enter 11 Digit No." required value={mobileNo} onChange={e => {setMobileNo(e.target.value)}} />
				</Form.Group>
				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" required value={password} onChange={e => {setPassword(e.target.value)}} />
				</Form.Group>
				<Form.Group>
					<Form.Label>Confirm Password:</Form.Label>
					<Form.Control type="password" placeholder="Confirm Password" required value={confirmPassword} onChange={e => {setConfirmPassword(e.target.value)}} />
				</Form.Group>
				{
					isActive
					? <Button variant="primary" type="submit" className="my-5">Submit</Button>
					:<Button variant="primary" disabled className="my-5">Submit</Button>
				}
			</Form>

			<Link to="../login">Already have an account? Login Here.</Link>

		</>
	)
}
