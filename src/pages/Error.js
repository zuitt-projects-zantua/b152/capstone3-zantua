import Banner from '../components/Banner';

export default function Error(){

	let errorBanner = {

		title: "Page Not Found",
		description: "The page you're looking for does not exist.",
		buttonText: "Back to Home",
		destination: "/"
	}


	return <Banner bannerProp={errorBanner}/>
}